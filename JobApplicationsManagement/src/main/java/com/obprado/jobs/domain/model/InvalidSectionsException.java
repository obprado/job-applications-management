/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obprado.jobs.domain.model;

/**
 *
 * @author omar
 */
public class InvalidSectionsException extends RuntimeException {

    public InvalidSectionsException() {
        super();
    }

    public InvalidSectionsException(String message) {
        super(message);
    }

    public InvalidSectionsException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidSectionsException(Throwable cause) {
        super(cause);
    }
    
}
