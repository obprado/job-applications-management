/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obprado.jobs.domain.model;

/**
 *
 * @author omar
 */
public class ApplicationsRepositoryMock implements ApplicationsRepository{

    private static int currentID = 0;
    
    @Override
    public String getNextID() {
        Integer current = ApplicationsRepositoryMock.currentID;
        ApplicationsRepositoryMock.currentID = ApplicationsRepositoryMock.currentID + 1;
        return current.toString();
    }
    
}
