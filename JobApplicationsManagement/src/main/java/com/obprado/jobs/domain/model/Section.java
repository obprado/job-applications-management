/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obprado.jobs.domain.model;

/**
 *
 * @author omar
 */
public class Section {
    
    private final int order;
    private final String title;
    private final String body;

    public Section(int order, String title, String body) {
        this.order = order;
        this.title = title;
        this.body = body;
    }

    public int getOrder() {
        return order;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }
    
    
    
}
