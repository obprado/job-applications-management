/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obprado.jobs.domain.model;

import java.util.Collection;
import java.util.LinkedList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author omar
 */
public class SectionsAnalizerTest {
    
    private Section section1;
    private Section section2;
    private Section section3;
    private Section section4;
    private Collection<Section> sections;
    
    public final static String TEST_TITLE = "test title";
    public final static String TEST_BODY = "test body";
    
    @Before
    public void setUp() {
        section1 = new Section(1, TEST_TITLE, TEST_BODY);
        section2 = new Section(2, TEST_TITLE, TEST_BODY);
        section3 = new Section(3, TEST_TITLE, TEST_BODY);
        section4 = new Section(4, TEST_TITLE, TEST_BODY);                
        sections = new LinkedList<>();
    }

    @Test
    public void correlativeShouldRejectCollectionsWithMissingOrders() {        
        sections.add(section1);
        sections.add(section3);
        sections.add(section4);
        SectionsAnalizer analizer = new SectionsAnalizer(sections);
        assertFalse("correlative() should reject collections if some order is missing", analizer.correlative());
    }
    
    @Test
    public void correlativeShouldAcceptCorrelativeSections() {        
        sections.add(section1);
        sections.add(section2);
        sections.add(section3);
        sections.add(section4);
        SectionsAnalizer analizer = new SectionsAnalizer(sections);
        assertTrue("correlative() should accept sections with correlative orders", analizer.correlative());
    }
    
}
