/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obprado.jobs.domain.model;

import java.util.Collection;

/**
 * Utility class used to check invariants on sections collections
 * @author omar
 */
public class SectionsAnalizer {
    
    private final Collection<Section> sections;

    public SectionsAnalizer(Collection<Section> sections) {
        this.sections = sections;
    }
    
    //This algorithm has a quadratic complexity O(n^2) provided that n is sections.size().
    //Should be refactored if the system requires accepting applications with a lot of sections.
    public Boolean correlative(){        
        boolean canBeOrdered = true;
        for(int currentOrder = 1; currentOrder <= sections.size(); currentOrder++){                       
            if (!hasSectionWithOrder(currentOrder)){
                canBeOrdered = false;
            }
        }
        return canBeOrdered;
    }
    
    private boolean hasSectionWithOrder(int order){
        boolean hasOrder = false;
        for(Section section : sections){
            if (section.getOrder() == order){
                hasOrder = true;
            }
        }
        return hasOrder;
    }
    
}
