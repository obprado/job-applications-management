/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obprado.jobs.domain.model;

import static org.junit.Assert.*;
import org.junit.*;

/**
 *
 * @author omar
 */
public class ApplicationsFactoryTest {
    
    private Company company1;
    private Company company2;
    private ApplicationsFactory factory;
    
    @Before
    public void init(){
        factory = new ApplicationsFactory();
        factory.setRepository(new ApplicationsRepositoryMock());
        company1 = new Company("1234", "test company 1");
        company2 = new Company("1234", "test company 2");
    }
    
    @Test
    public void shouldAssignID(){
        Application application = factory.create(company1, "test position");
        assertNotNull("The factory should assign an ID to the company", application.getID());        
    }
    
    @Test
    public void IDsAssignedShouldBeUnique(){        
        Application application1 = factory.create(company1, "test position");
        Application application2 = factory.create(company2, "test position");
        assertNotEquals("IDs should be unique", application1.getID(), application2.getID());
    }
    
    @Test
    public void shouldHaveNoSections(){
        Application application1 = factory.create(company1, "test position");
        assertTrue("shouldn't have sections", application1.getSections().isEmpty());
    }
    
}



