/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obprado.jobs.domain.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author omar
 */
public class ApplicationTest {

    private Application application;
    private Section section1;
    private Section section2;
    private Section section3;
    private Section section4;
    private Section anotherSection2;

    public final static String TEST_TITLE = "test title";
    public final static String TEST_BODY = "test body";

    @Before
    public void init() {
        application = new Application("1", new Company("test id", "test comp."), "test position", new ArrayList<Section>());
        section1 = new Section(1, TEST_TITLE, TEST_BODY);
        section2 = new Section(2, TEST_TITLE, TEST_BODY);
        section3 = new Section(3, TEST_TITLE, TEST_BODY);
        section4 = new Section(4, TEST_TITLE, TEST_BODY);
        anotherSection2 = new Section(2, TEST_TITLE, TEST_BODY);
    }

    @Test
    public void appendSectionShouldAssignCorrelativeOrder() {
        application.appendSection(TEST_TITLE, TEST_BODY);
        assertEquals("order should be correlative", 1, application.getSections().iterator().next().getOrder());
    }

    @Test
    public void appendSectionShouldAssignTitleAndBody() {
        application.appendSection(TEST_TITLE, TEST_BODY);
        assertEquals("title should be the one provided", TEST_TITLE, application.getSections().iterator().next().getTitle());
        assertEquals("body should be the one provided", TEST_BODY, application.getSections().iterator().next().getBody());
    }

    @Test
    public void overrideSectionsShouldReplaceTheSections() {
        application.appendSection(TEST_TITLE, TEST_BODY);
        application.appendSection(TEST_TITLE, TEST_BODY);
        Collection<Section> sections = new LinkedList<>();
        sections.add(section1);
        sections.add(section2);
        sections.add(section3);
        sections.add(section4);
        application.overrideSections(sections);
        assertEquals("the application should have the new sections", 4, application.getSections().size());
    }

    @Test
    public void overrideSectionsShouldRequireASectionWithOrderOne() {
        try {
            Collection<Section> sections = new LinkedList<>();
            sections.add(section2);
            sections.add(section3);
            application.overrideSections(sections);
            fail("Override sections should throw an Exception when there is no Section with order one");
        } catch (InvalidSectionsException e) {
            //It is ok to reach here
        }
    }
    
    @Test
    public void overrideSectionsShouldRequireCorrelativeOrders(){
        try {
            Collection<Section> sections = new LinkedList<>();
            sections.add(section1);
            sections.add(section3);
            application.overrideSections(sections);
            fail("Override sections should throw an Exception when there is a missing order. It should be possible to order sections like 'order: 1, order:2, order: 3,... order: n'");
        } catch (InvalidSectionsException e) {
            //It is ok to reach here
        }
    }
    
    @Test
    public void overrideSectionsShouldNotAllowDuplicateOrders(){
        try {
            Collection<Section> sections = new LinkedList<>();
            sections.add(section1);
            sections.add(section2);
            sections.add(anotherSection2);
            sections.add(section3);
            application.overrideSections(sections);
            fail("Override sections should throw an Exception when there are sections with duplicated orders");
        } catch (InvalidSectionsException e) {           
            //It is ok to reach here
        }
    }

}
