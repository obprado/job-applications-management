/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obprado.jobs.domain.model;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author omar
 */
public class ApplicationsFactory {
    
    @Autowired
    private ApplicationsRepository repository;

    public Application create(Company company, String position){
        return new Application(repository.getNextID(), company, position, new ArrayList<Section>());
    }

    void setRepository(ApplicationsRepository repository) {
        this.repository = repository;
    }
    
}
