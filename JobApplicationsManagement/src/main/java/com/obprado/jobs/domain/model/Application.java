/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.obprado.jobs.domain.model;

import java.util.Collection;

/**
 * Entity representing a job application. 
 * It's the root of an Aggregate containing several sections.
 * @author omar
 */
public class Application {
    
    private final String ID;
    private final Company company;
    private String position;
    private Collection<Section> sections;

    public Application(String ID, Company company, String position, Collection<Section> sections) {
        this.ID = ID;
        this.company = company;
        this.position = position;
        this.sections = sections;
    }

    public String getID() {
        return ID;
    }

    public Company getCompany() {
        return company;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Collection<Section> getSections() {
        return sections;
    }
    
    public void appendSection(String title, String body){
        int order = this.sections.size() + 1;
        Section section = new Section(order, title, body);
        this.sections.add(section);
    }
    
    public void overrideSections(Collection<Section> newSections){
        SectionsAnalizer analizer = new SectionsAnalizer(newSections);
        if (!analizer.correlative()){
            throw new InvalidSectionsException("Sections should have correlative order");
        }
        this.sections = newSections;
    }
    
}
